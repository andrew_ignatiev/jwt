## Description

JWT token generator

## Installation

```bash
$ npm install
```

## Private/Public Key generation
```angular2html
Please use online tool to generate keys href="http://travistidwell.com/jsencrypt/demo/
Then update public.key and private.key files accordingly
```

## Generate JWT

```bash
$ node index.js
```
