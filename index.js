const fs = require('fs');
const jwt = require('jsonwebtoken');

// PAYLOAD
const payload = {
  roles: ['customer'],
  storeId: 1,
  data1: 'Data 1',
  data2: 'Data 2',
  data3: 'Data 3',
  data4: 'Data 4',
};

// PRIVATE and PUBLIC key
const privateKEY = fs.readFileSync('./private.key', 'utf8');
const publicKEY = fs.readFileSync('./public.key', 'utf8');

const i = 'SportPursuit'; // Issuer
const s = '55555'; // Subject
const a = 'https://www.sportpursuit.com'; // Audience

// SIGNING OPTIONS
const signOptions = {
  issuer: i,
  subject: s,
  audience: a,
  expiresIn: '1h',
  algorithm: 'RS256',
};

const token = jwt.sign(payload, privateKEY, signOptions);

console.log(token);
